package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"log"
	"strconv"
	//"strconv"
	"strings"
)

const (
	bigNumber   = 10000
	divider     = 16
	numSequence = 16
	sizeByte    = 8
)

var (
	walshSequence = [16]int{1, 1, -1, -1,  1,  1, -1, -1, 1, 1, -1, -1, 1, 1, -1, -1}
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to read row with printing of message */
func readRow(mess string, row *string) {

	/* declarations of variable */
	var (
		err error
	)

	/* print message and read row */
	fmt.Println(mess)
	_, err = fmt.Scanln(row)
	handleError(err)

}

func readFile(filename string) []int {

	/* declarations of variables */
	var (
		err        error
		reader     *bufio.Reader
		file       *os.File
		length     int
		line       []byte
		row        string
		array      []int
		substrings []string
		i          int
		newElem    int
	)

	/* create array */
	array = make([]int, 0, bigNumber)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* initial length */
	length = 0
	/* create reader and read file */
	reader = bufio.NewReader(file)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		substrings = strings.Fields(row)
		length += len(substrings)

		/* receive new numbers */
		i = 0
		for i < len(substrings) {
			newElem, err = strconv.Atoi(substrings[i])
			handleError(err)
			array = append(array, newElem)
			i++
		}

	}

	/* close file */
	err = file.Close()
	handleError(err)

	return array

}

/* function to do Walsh Transformation */
func walshTransform(array []int) []int {

	var (
		n      int
	    i      int
		i1     int
		answer []int
		sum    int
	)

	/* initialization arrays */
	answer = make([]int, len(array) / numSequence)
	n = len(array) / numSequence

	/* do transformation */
	i = 0
	for i < n {
		i1 = 0
		sum = 0
		for i1 < numSequence {
			sum += array[numSequence * i + i1] * walshSequence[i1]
			i1++
		}
		sum /= divider
		answer[i] = (1 - sum) / 2
		i++
	}

	/* return answer */
	return answer

}

/* function to write results */
func writeResults(answer []int) {

	var (
		i        int
		byteInt  int
		byteTrue byte
	)

	/* write bits */
	fmt.Println("Bits:")
	i = 0
	for i < len(answer) {
		fmt.Print(answer[i])
		fmt.Print(" ")
		i++
	}
	fmt.Println()

	/* write byte */
	fmt.Println("Byte:")
	i = 0
	for i < len(answer) / sizeByte {
		byteInt = 128 * answer[sizeByte * i] + 64 * answer[sizeByte * i + 1] + 32 * answer[sizeByte * i + 2] +
			16 * answer[sizeByte * i + 3] + 8 * answer[sizeByte * i + 4] + 4 * answer[sizeByte * i + 5] +
			2 * answer[sizeByte * i + 6] + answer[sizeByte * i + 7]
		fmt.Print(byteInt)
		fmt.Print(" ")
		i++
	}
	fmt.Println()

	/* write ASCII code */
	fmt.Println("ASCII:")
	i = 0
	for i < len(answer) / sizeByte {
		byteInt = 128 * answer[sizeByte * i] + 64 * answer[sizeByte * i + 1] + 32 * answer[sizeByte * i + 2] +
			16 * answer[sizeByte * i + 3] + 8 * answer[sizeByte * i + 4] + 4 * answer[sizeByte * i + 5] +
			2 * answer[sizeByte * i + 6] + answer[sizeByte * i + 7]
		byteTrue = byte(byteInt)
		fmt.Printf("%c", byteTrue)
		i++
	}
	fmt.Println()

}

/* entry point */
func main() {

	var (
		filename string
		array    []int
		answer   []int
	)

	/* read row */
	readRow("Name of file:", &filename)
	/* read file */
	array = readFile(filename)
	/* do Walsh transformation */
	answer = walshTransform(array)
	/* write results */
	writeResults(answer)

}